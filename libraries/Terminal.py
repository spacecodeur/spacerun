from termcolor import colored
import os
import threading
import time
import signal
import sys

class Terminal:
    
    @staticmethod
    def showSpacerunTitle() -> None:
        print("       _____\n      / ____|                                           /\\\n     | (___  _ __   __ _  ___ ___ _ __ _   _ _ __      /  \\   _ __  _ __\n      \\___ \\| '_ \\ / _` |/ __/ _ \\ '__| | | | '_ \\    / /\\ \\ | '_ \\| '_ \\\n      ____) | |_) | (_| | (_|  __/ |  | |_| | | | |  / ____ \\| |_) | |_) |\n     |_____/| .__/ \\__,_|\\___\\___|_|   \\__,_|_| |_| /_/    \\_\\ .__/| .__/\n ______     | |                                              | |   | |______\n|______|    |_|                                              |_|   |_|______|")


    @staticmethod
    def title(title: str) -> str:
        return colored(" " + title + " ", "black", "on_green")
    
    @staticmethod
    def exercice(exercice: str) -> str:
        return colored(" " + exercice + " ", "black", "on_blue")
    
    @staticmethod
    def showProgress(current: int, total: int) -> None:
        siteProgressBar = 50
        charFill = "▓"
        charEmpty = "-"

        res = "|"

        for i in range(siteProgressBar):
            if(i < ( current / total ) * siteProgressBar ):
                res += charFill  
            else:
                res += charEmpty
        
        res += "| " + str(round((current / total) * 100, 2)) + "%"

        s = ""
        if(current > 1):
            s = "s"

        print(f"\n{Terminal.title('Progression générale :')} {res} ({current} exercice{s} sur {total})\n")
    
    @staticmethod
    def clear():
        os.system('cls' if os.name == 'nt' else 'clear')


    @staticmethod
    def showTitleOfExercice(exercice: str, indexExercice: int) -> None:
        print(f"{Terminal.title('Exercice nº' + str(indexExercice + 1))} {Terminal.exercice(exercice)}\n")

    @staticmethod
    def _triggerHint(runInstance):
        while True:
            
            user_input = input()
            if user_input.lower() == 'hint':\
                # [1] opération un peu risquy étant donné qu'on sollicite le filesystem + lecture d'un fichier, mais...
                hintBloc = runInstance.currentExerciceHasHintBloc() # [2] ...temps d'execution moyen : ~ 0.0002 sec, et...
                if hintBloc != False:
                    print(Terminal.title("Aide"))
                    print(hintBloc)
                elif hintBloc == False:
                    print(f"Pas d'aide pour cet exercice !")

            time.sleep(0.4) # [2]...le tour de boucle (0.4) est largement supérieur à ~0.0002 sec

    @staticmethod
    def hintListener(runInstance):
        def signal_handler(sig, frame):
            sys.exit(0) # nécessaire pour kill proprement le thread qui se s'occupe du hint lorsque le programme est stopé

        signal.signal(signal.SIGINT, signal_handler)

        hint_thread = threading.Thread(target=Terminal._triggerHint, daemon=True, args=(runInstance,))
        hint_thread.start()