import os
from typing import Dict, List


class Filesystem():
    
    @staticmethod
    def isExistingFile(filePath: str) -> bool:
        return os.path.isfile(filePath)
    
    @staticmethod
    def containAtLastOneSubDirectory(directoryPath: str) -> bool:
        for _, dirs, _ in os.walk(directoryPath):
            if dirs:
                return True
        return False

    @staticmethod
    def containAtLastOneSubFile(directoryPath: str) -> bool:
        for _, _, files in os.walk(directoryPath):
            if files:
                return True
        return False

    @staticmethod
    def getAllRunsFromFileSystem(directoryParentPath: str):
        result = []
        for dossier in os.listdir(directoryParentPath):
            path = os.path.join(directoryParentPath, dossier)
            if os.path.isdir(path):
                for sous_dossier in os.listdir(path):
                    sous_path = os.path.join(path, sous_dossier)
                    if (
                        os.path.isdir(sous_path) and
                        Filesystem.isExistingFile(sous_path + "/requirements.py")
                        and Filesystem.containAtLastOneSubDirectory(sous_path)
                        and Filesystem.containAtLastOneSubFile(sous_path)
                    ):
                        result.append(sous_path)
        return result

    
    @staticmethod
    def getExercices(mainDirPath: str) -> Dict[str, List[str]]:
        exercicesStruct: List[str] = []

        for category in os.listdir(mainDirPath):
            categoryPath = os.path.join(mainDirPath, category)
            if os.path.isdir(categoryPath):
                for exerciceFile in os.listdir(categoryPath):
                    exercicesStruct.append(os.path.join(category, exerciceFile))

        exercicesStruct.sort()    
        return exercicesStruct
    
    @staticmethod
    def checkIfFileIsEmpty(filePath: str) -> bool:
        return os.stat(filePath).st_size == 0
    
    @staticmethod
    def copyContentFile1ToFile2(filePath1: str, filePath2: str, file2MustBeEmpty: bool = False) -> None:
        if file2MustBeEmpty and os.path.exists(filePath2) and os.stat(filePath2).st_size > 0:
            return
        with open(filePath1, 'r') as file1:
            with open(filePath2, 'w') as file2:
                hintDetected = False
                for line in file1:
                    if '<<{hint}>>' in line:
                        hintDetected = True
                        continue
                    if not hintDetected:
                        file2.write(line)

    @staticmethod
    def fileNameStartWith(relativeBaseDirPath: str, startWith: str) -> str:
        files = os.listdir(relativeBaseDirPath)
        for fileName in files:
            if fileName.startswith(startWith):
                return os.path.join(relativeBaseDirPath, fileName)
        raise FileNotFoundError("Fichier EXERCICE non trouvé")

    @staticmethod
    def executeCommand(command: str) -> int :
        return os.system(command)