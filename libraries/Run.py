from typing import List, Self, Tuple, Union
from libraries.Filesystem import Filesystem

class Run():

    def __init__(self: Self, choosenRun: str) -> Self:
        self.choosenRun: str = choosenRun
        self.exercices: List[str] = self.addExercices()

    @staticmethod
    def isValidRun(relativeBaseDirPath: str) -> bool :
        relativeBaseDirPath = 'runs/' + relativeBaseDirPath 
        return (
            Filesystem.isExistingFile(relativeBaseDirPath + "/requirements.py")
            and Filesystem.containAtLastOneSubDirectory(relativeBaseDirPath)
            and Filesystem.containAtLastOneSubFile(relativeBaseDirPath)
        )
    
    def installRequirements(self: Self) -> None:
        Filesystem.executeCommand("python " + self.choosenRun + "/requirements.py  > /dev/null 2>&1")
    
    def addExercices(self: Self) -> List[str]:
        # retourne par exemple "00_les-variables/00_les-variables.js"
        return Filesystem.getExercices(self.choosenRun)

    def getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment(self: Self) -> Union[Tuple[int, str], None]:
        for key, exercice in enumerate(self.exercices):
            if(self.exerciceContain_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment(self.choosenRun + "/" + exercice) == True):
                return key, self.exercices[key]
        return None

    def exerciceContain_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment(self: Self, exercicePath: str = None) -> bool:
        if(exercicePath == None):
            _, exercice = self.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()
            exercicePath = self.choosenRun + "/" + exercice
        with open(exercicePath, 'r') as f:
            lignes = f.readlines()
            if any("DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE" in ligne for ligne in lignes):
                return True
            return False
        
    def workspaceContain_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment(self: Self):
        with open(self.getWorkspaceFilePath(), 'r') as f:
            lignes = f.readlines()
            if any("DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE" in ligne for ligne in lignes):
                return True
            return False
    
    def getWorkspaceFilePath(self: Self) -> str:
        return Filesystem.fileNameStartWith(self.choosenRun, "workspace")

    def isWorkspaceFileEmpty(self: Self) -> bool:
        return Filesystem.checkIfFileIsEmpty(self.getWorkspaceFilePath()) == True

    def copyCurrentExerciceToWorkspaceFile(self: Self, file2MustBeEmpty:bool = True) -> None :
        _, exercice = self.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()
        Filesystem.copyContentFile1ToFile2(
            self.choosenRun + "/" + 
            exercice, 
            self.getWorkspaceFilePath(), file2MustBeEmpty)
        
    def copyWorkspaceFileToCurrentExercice(self: Self) -> None :
        _, exercice = self.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()
        Filesystem.copyContentFile1ToFile2( 
            self.getWorkspaceFilePath(),
            self.choosenRun + "/" + 
            exercice, False)
    
    def executeAssertsOnWorkspaceFile(self: Self) -> int :
        if '/' in self.choosenRun:
            sep = '/' # unix
        else:
            sep = '\\' # windows
        language = self.choosenRun.split(sep)[1]
        if (language == "js"):
            return Filesystem.executeCommand("node " + self.getWorkspaceFilePath())
        elif(language == "python"):
            return Filesystem.executeCommand("python " + self.getWorkspaceFilePath())
        
    def installRequirements(self: Self) -> None:
        Filesystem.executeCommand("python " + self.choosenRun + "/requirements.py > /dev/null 2>&1")

    def currentExerciceHasHintBloc(self: Self) -> Union[str, False]:
        _, exercice = self.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()
        with open(self.choosenRun + "/" + exercice, 'r') as f:
            lignes = f.readlines()
            hintBlock = ""
            hintStarted = False
            for ligne in lignes:
                if "<<{hint}>>" in ligne:
                    hintStarted = True
                    continue
                if hintStarted == True:
                    hintBlock += ligne
            if hintBlock != "":
                return hintBlock.strip()  # Return hint block without leading/trailing whitespaces
        return False