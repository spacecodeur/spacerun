from libraries.Filesystem import Filesystem
from libraries.Run import Run
from libraries.Terminal import Terminal
from watchfiles import watch
import questionary

Terminal.clear()
Terminal.showSpacerunTitle()

# choix du run
print(Terminal.title("Choisissez un langage: (valider avec la touche entrée)"))
choosenRun = questionary.select(
    "",
    choices=Filesystem.getAllRunsFromFileSystem("runs")
).ask()

# instanciation d'un run
runInstance = Run(choosenRun)

Terminal.hintListener(runInstance)

# on installe les requirements en fonction du run choisi
runInstance.installRequirements()

# avant de démarrer : on copy dans le workspace le dernier exercice et on lance les tests
if(runInstance.isWorkspaceFileEmpty() == True):
    runInstance.copyCurrentExerciceToWorkspaceFile()

Terminal.clear()
indexExercice, exercice = runInstance.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()

if(exercice != None):
    Terminal.showProgress(indexExercice, len(runInstance.exercices))
    Terminal.showTitleOfExercice(exercice, indexExercice)

    # on exécute les tests automatiquement une fois avant le moindre changement
    runInstance.executeAssertsOnWorkspaceFile()

    try:
        # workflow d'un run
        for changes in watch(runInstance.getWorkspaceFilePath()):
            # le fichier workspace a été modifié
            Terminal.clear()
            indexExercice, exercice = runInstance.getFirstExerciceWith_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment()
            Terminal.showProgress(indexExercice, len(runInstance.exercices))
            Terminal.showTitleOfExercice(exercice, indexExercice)

            if(runInstance.isWorkspaceFileEmpty() == True):
                runInstance.copyCurrentExerciceToWorkspaceFile()
                
            result: int = runInstance.executeAssertsOnWorkspaceFile()
            
            if(result == 0): # les tests sont OK :D
                if(runInstance.workspaceContain_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment() == True):
                    print(Terminal.title("Exercice terminé ! retire le commentaire 'DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE' tout en haut puis appuie sur CTRL+s si tu veux passer à l'exercice suivant !"))
                else:
                    runInstance.copyWorkspaceFileToCurrentExercice()
                    runInstance.copyCurrentExerciceToWorkspaceFile(False)
            elif(runInstance.workspaceContain_DELETE_THIS_COMMENT_FOR_PASSING_TO_THE_NEXT_EXERCICE_comment() == False):
                # retirer le commentaire "DELETE THIS ..." permet de passer à l'exercice suivant, même si des erreurs persistent
                runInstance.copyWorkspaceFileToCurrentExercice()
                runInstance.copyCurrentExerciceToWorkspaceFile(False)
    except KeyboardInterrupt:
        Terminal.clear()
        print("\n" + Terminal.title("Bye ! à la prochaine ! :D") + "\n")

else:
    print("Tous les exercices ont été terminé ! GG à vous !")