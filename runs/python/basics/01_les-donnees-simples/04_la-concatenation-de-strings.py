# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

# il est important de se poser toujours la question des types 
# des variables qu'on manipule à travers nos programmes sous 
# peine de provoquer des bugs non voulus 

# Par exemple selon le type de variable que vous manipulez en python, 
# l'opération + n'aura pas du tout le même effet, par exemple : 

a = 5
b = 6
c = 'bonjour' # remarquez qu'en python, les string peuvent être déclaré avec des " ou des '
d = "toto"

resultat1 = a + b # resultat1 a alors comme valeur 11 car l'operation + a été effectuée entre deux nombres
resultat2 = c + d # resultat2 a alors comme valeur "bonjourtoto" car l'operation + a été effectuée entre deux chaines de caractères

# dans le cas c + d, on dit qu'on a concaténé deux chaines de caractères, ce qui revient à les coller côte à côte


##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# déclarez 3 variables, chacune de ces variables doit contenir une chaine de caracteres

# puis concaténez les 3 variables ensemble, le résultat de la concaténation doit être stocké dans la variable concatenationDesVariables
concatenationDesVariables = None

variableSup = 42 # modifiez le type de cette variable pour faire fonctionner la suite

print(concatenationDesVariables + variableSup) # ne pas modifier cette ligne

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert isinstance(concatenationDesVariables, str), "concatenationDesVariables doit être une chaine de caractère"
assert isinstance(variableSup, str), "variableSup doit être une chaine de caractère"
assert concatenationDesVariables + variableSup == concatenationDesVariables + "42"