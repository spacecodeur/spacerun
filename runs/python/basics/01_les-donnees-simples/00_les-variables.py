# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

# dans un programme, nous pouvons demander poliment à notre ordinateur de sauvegarder dans sa mémoire une donnée
# dans ces premiers exercices, nous nous concentrerons sur des données 'simples'

# demandons sans plus attendre à notre ordinateur de stocker temporairement un nombre entier dans un espace mémoire nommé 'x'
x = 11

# en programmation, 'x' est ce qu'on appelle une variable
# nous pouvons afficher le contenu d'une variable comme ceci
print(x) # cette ligne affichera 11 dans le terminal

# l'utilisation de variable est la base de la plupars des langages de programmation !

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# compléte le code ci-dessous pour retirer les erreurs et affichez les valeurs des variables a, b et c
a = 
b = 22.5
c = 

print() # je veux afficher la valeur de la variable a
print() # je veux afficher la valeur de la variable b
print() # je veux afficher la valeur de la variable c