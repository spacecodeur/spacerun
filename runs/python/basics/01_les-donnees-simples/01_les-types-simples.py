# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

# nous pouvons stocker dans des variables toute sorte de données 
# voici quelques exemples de données dîtes 'simples' ; les nombres, les lettres (char) et les mots (string) ou encore les booléens (boolean : qui peut être soit de valeur Vrai, soit de valeur Faux)

jeSuisUneVariableQuiStockeUnNumber = 10
jeSuisUneVariableQuiStockeUnString = "bonjour !" # un string est une suite de char :) 
jeSuisUneVariableQuiStockeUnBoolean = True

# tu peux remarquer qu'il n'y a pas de règle imposée pour donner un nom à une variable
# mais il faut idéalement que le nom soit explicite, imagine si un collegue lit ton code, il doit savoir rapidement à quel type de variable il a à faire. Bien nommer ses variables facilite la relecture et un code bien écrit accroit grandement les chances de se faire offrir un café / chocolat chaud !

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

nomDeLaville = 
temperatureActuelle = 22 # ne pas modifier cette ligne

print("Bienvenue sur " + nomDeLaville)
print("La temperature actuelle est de " + temperatureActuelle) # une erreur apparait à cette ligne, et si vous recherchiez la réponse sur Internet via votre moteur de recherche préféré ? tape "hint" sur le terminal où spacerun est lancé est appuie sur la touche entree pour afficher des astuces sur cet exercice

# <<{hint}>>
# il est possible d'afficher des aides pour certains exercices. N'utilise cette fonctionnalité que si tu bloques depuis un certain temps déjà
# pour cet exercice, voici quelques tips pour lorsque tu effectues une recherche sur Internet :
# - n'utilise que des mots clé dans ta recherche (pas de mot de liaison type "de", "le", ...)
# - contextualise ta recherche; tu es en train de faire de la programmation, et en python sur le sujet de l'affichage de variable avec print
# - effectue une recherche en anglais (la majeur partie des contenus sont en anglais, donc une recherche en anglais donnera de meilleures chances de trouver de bons résultats)
# - recoupe les sources que tu croises, un site semble donner la bonne explication => regarde si tu retrouves cette explication sur d'autres sites
# - voici un exemple de recherche pour l'erreur liée à la temperature : python variable print error can only concatenate str not int 