# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# Déclarez une variable 'prenom' et assignez-lui votre prénom


# Déclarez une variable 'age' et assignez-lui votre âge


# Déclarez une variable 'pays' et assignez-lui le pays où vous vivez


# Déclarez une variable 'estEtudiant' et assignez-lui une valeur booléenne indiquant si vous êtes étudiant ou non


##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'prenom' in locals(), "La variable 'prenom' n'est pas définie"
assert isinstance(prenom, str), "Remplissez la variable nom avec votre prénom."
assert 'age' in locals(), "La variable 'age' n'est pas définie"
assert isinstance(age, int), "Remplissez la variable age avec votre âge."
assert 'pays' in locals(), "La variable 'pays' n'est pas définie"
assert isinstance(pays, str), "Remplissez la variable pays avec le pays où vous vivez."
assert 'estEtudiant' in locals(), "La variable 'estEtudiant' n'est pas définie"
assert isinstance(estEtudiant, bool), "Remplissez la variable estEtudiant avec True si vous êtes étudiant, False sinon."

print(f"je m'appelle {prenom}, j'ai {age} ans !")
print(f"j'habite en {pays}")
if estEtudiant == True:
    print("et je suis étudiant(e) !") 
