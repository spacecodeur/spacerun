# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

# il est possible d'effectuer des calculs basiques entre des nombres
# et de stocker le résultat dans des variables

# dans la plupars des langages de programmations, les opérations suivantes sont mises à disposition : 
# addiction +, soustraction -, multiplication *, division /, et le modulo % (le modulo est le reste de la division euclidienne entre deux nombres)

# voici deux exemples d'utilisation d'opérations en python
resultat1 = 17 + 12 # nous stockons dans la variable resultat1 la valeur 29

a = 10
b = 7
resultat2 = a - b # nous stockons dans la variable resultat2 la valeur 3 (car 10 - 7 est égal à 3)

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

 # dans les lignes ci-dessous, modifie seulement la valeur des variables pour faire fonctionner les tests

nombreA = None # None est une valeur pour indiquer qu'une variable n'a ni de type, ni de valeur. Dans cet exercice, il faut modifier tous les None par un nombre entier et passer les tests définis plus bas
nombreB = None
nombreC = None
nombreD = None
nombreE = None
nombreF = None
nombreG = None
nombreH = None
nombreI = None
nombreJ = None
nombreK = None
nombreL = None
nombreM = None
nombreN = None

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'nombreA' in locals() and isinstance(nombreA, int), "nombreA doit etre défini et être un nombre entier"
assert 'nombreB' in locals() and isinstance(nombreB, int), "nombreB doit etre défini et être un nombre entier"
assert 'nombreC' in locals() and isinstance(nombreC, int), "nombreC doit etre défini et être un nombre entier"
assert 'nombreD' in locals() and isinstance(nombreD, int), "nombreD doit etre défini et être un nombre entier"
assert 'nombreE' in locals() and isinstance(nombreE, int), "nombreE doit etre défini et être un nombre entier"
assert 'nombreF' in locals() and isinstance(nombreF, int), "nombreF doit etre défini et être un nombre entier"
assert 'nombreG' in locals() and isinstance(nombreG, int), "nombreG doit etre défini et être un nombre entier"
assert 'nombreH' in locals() and isinstance(nombreH, int), "nombreH doit etre défini et être un nombre entier"
assert 'nombreI' in locals() and isinstance(nombreI, int), "nombreI doit etre défini et être un nombre entier"
assert 'nombreJ' in locals() and isinstance(nombreJ, int), "nombreJ doit etre défini et être un nombre entier"
assert 'nombreK' in locals() and isinstance(nombreK, int), "nombreK doit etre défini et être un nombre entier"
assert 'nombreL' in locals() and isinstance(nombreL, int), "nombreL doit etre défini et être un nombre entier"
assert 'nombreM' in locals() and isinstance(nombreM, int), "nombreM doit etre défini et être un nombre entier"
assert 'nombreN' in locals() and isinstance(nombreN, int), "nombreN doit etre défini et être un nombre entier"
assert nombreA + nombreB == 10, "nombreA + nombreB doit donner comme résultat 10"
assert nombreC - nombreD == 17, "nombreC - nombreD doit donner comme résultat 17"
assert nombreE * nombreF == 12, "nombreE * nombreF doit donner comme résultat 12"
assert nombreG / nombreH == 2, "nombreG / nombreH doit donner comme résultat 2"
assert nombreI % nombreJ == 7, "nombreI % nombreJ doit donner comme résultat 7"
assert nombreK % nombreL == 1, "nombreK % nombreL doit donner comme résultat 1"
assert nombreM % nombreN == 0, "nombreM % nombreN doit donner comme résultat 0"

# <<{hint}>>
# Des difficultés avec le modulo % ? c'est normal, bien qu'il s'agisse d'une opération basique en programmation, ce n'est pas une opération qu'on a l'habitude d'effectuer en dehors de nos lignes de code
# Voici des exemples, avec explications, du modulo : 
# 17 modulo 5 est égal à 2, car, 2 est le reste de la division euclidienne entre 17 et 5. En effet, 17 est égal à 3 * 5 (15) et le reste est donc 2
# 9 modulo 5 est égal à 4, car, 9 = 5 * 1 + 4
# 11 modulo 4 est égal à 3, car, 11 = 4 * 2 + 3
# pour terminer, le dernier exemple peut se traduire en code python de la manière suivante : 
# resultat = 11 % 4
# nous stockons alors dans la variable resultat la valeur 3, car 3 (comme vu juste au dessus) est le reste de la division euclidienne entre 11 et 4