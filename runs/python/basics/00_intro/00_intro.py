# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

print("\nHey ! salutation jeune spacerunner !\nComment fonctionne un spacerunning ? il y a deux choses à observer pendant un run'\n\nLe terminal que tu es en train de lire : dans ce terminal seront affichées des erreurs, le but du jeu est de résoudre ces erreurs !\n\nComment les résoudre ? Il te suffit d'ouvrir, depuis ton IDE préféré, le fichier runs/python/workspace.py\n-> et dans ce fichier se trouvera le code à analyser et à débugger/compléter\n-> et c'est le seul fichier que tu as besoin d'ouvrir ;) (ce fichier se met automatiquement à jour lorsqu'un nouvel exercice se charge)\n\nAprès avoir apporté des modifications dans le fichier workspace, sauvegardes ton travail (ctrl+s pour sauvegarder), la sauvegarde redéclenche automatiquement les tests de vérification du code...\n...et lorsqu'il n'y a plus d'erreur dans le code (le terminal te l'indiquera), tu pourras enchainer directement sur l'exercice suivant en retirant le commentaire '# DELETE THIS COMMENT FOR...' situé tout en haut du fichier (plus un p'tit ctrl+s)\n\nAllez ! place aux erreurs à résoudre ! n'oublis pas, tu ne dois travailler QUE sur le fichier runs/python/workspace.py\nBon run :D\n")

# ceci est un commentaire (écrit grace au caractère #), un commentaire n'est jamais exécuté par le programme. Il s'agit juste d'une indication pour l'équipe qui développe le programme
# lisez bien les commentaires qui seront inscrits dans le fichier workspace dans chaque exercice ! il vous aideront à avancer

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

jaiPasToutLu = True # modifie ici True par False si tu as lu tout le contenu du fichier workspace !
jaiToutCompris = False # modifie ici False par True si tu as compris tout ce que as lu ! 

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert isinstance(jaiPasToutLu, bool), "La variable n'est pas de type booléen"
assert jaiPasToutLu == False, "Si tu as lu le texte ci-dessus, tu dois mettre cette variable à True dans le fichier runs/python/workspace.py"
assert isinstance(jaiToutCompris, bool), "La variable n'est pas de type booléen"
assert jaiToutCompris == True, "Si tu as lu le texte ci-dessus et que tu as tout compris, tu dois mettre cette variable à True dans le fichier runs/python/workspace.py"