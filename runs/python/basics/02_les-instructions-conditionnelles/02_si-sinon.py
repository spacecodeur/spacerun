# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

# L'instruction conditionnelle else est utilisée en programmation pour exécuter un bloc de code
# lorsque la condition d'un if n'est pas vraie.

# Voici un exemple simple d'utilisation de l'instruction else :

age = 20

if age >= 18:
    print('Tu es majeur.')
else:
    print('Tu es mineur.')

# Dans cet exemple, si l'âge est supérieur ou égal à 18, le programme affichera "Tu es majeur.",
# sinon, il affichera "Tu es mineur."

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# Dans les lignes ci-dessous, complète le code en suivant les indications commentées
# Il est interdit d'ajouter un nouveau if 

motDePasse = '218c45438eba'
accesRefuse = True # ne pas modifier cette ligne

if motDePasse != '218c45438eba':
    print('Accès refusé !')
# completer le code ici : ajouter un else
    # dans le else, affecter la valeur False à la variable accesRefuse

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# Tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'motDePasse' in locals(), "la variable motDePasse doit être définie"
assert isinstance(motDePasse, str), "motDePasse doit être de type str"
assert accesRefuse == False, "à la fin de ce programme, l'accès doit être autorisé, la variable accesRefuse doit avoir comme valeur False si le mot de passe est '218c45438eba'"