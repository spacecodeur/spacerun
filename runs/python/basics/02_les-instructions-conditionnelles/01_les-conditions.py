# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# dans le cadre de cet exercice, seules les deux lignes ci-dessous peuvent être modifiées

age = 15
argent = 50

# NE MODIFIE PAS LES LIGNES SUIVANTES

estAdulte = False
estEtudiant = False
laBoutiqueEstVide = False

if age >= 18:
    estAdulte = True

if age <= 21:
    estEtudiant = True

if estAdulte and argent >= 100:
    print('vendeur : tu as acheté tout mon stock !')
    laBoutiqueEstVide = True
    argent = 20

    if estEtudiant == True or age >= 60:
        print('vendeur : attends ! tu as droit à une réduction sur le prix ! je te rends 40 du coup')
        argent = 40
        

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'age' in locals(), "la variable age doit être définie"
assert isinstance(age, int), "age doit être un nombre entier"
assert 'argent' in locals(), "la variable argent doit être définie"
assert isinstance(age, int), "argent doit être un nombre entier"
assert laBoutiqueEstVide == True, "ton objectif ici est de vider la boutique ! trouve un moyen pour que le la variable laBoutiqueEstVide ait pour valeur True"
assert argent == 40, "à la fin de ce programme, argent doit être égal à 40, analyse le code pour trouver comment faire !"
