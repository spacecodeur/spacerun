# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

import random

force = random.randint(3, 18) # force contient un nombre aléatoire entre 3 et 18

# Écrire le code correspondant aux pseudo-code ci-dessous
#
# Définir une variable nommée classe, cette variable contient pour le moment la valeur None
# Définir une variable nommée prenom, cette variable contient le prenom de votre personnage
# Définir une variable nommée intelligence, cette variable doit contenir un nombre aléatoirement compris entre 3 et 18
# Définir une variable nommée dexterite, cette variable doit contenir un nombre aléatoirement compris entre 3 et 18
#
# SI la force ET l'intelligence sont supérieurs à 12
#    ALORS classe doit avoir pour valeur "mage de combat"
# SINON SI l'intelligence ET la dextérité sont supérieurs à 12
#    ALORS classe doit avoir pour valeur "assassin"
# SINON
#    ALORS classe soit avoir pour valeur "grouillot"
#
# SI la force OU l'intelligence OU la dextérité est supérieure à 15
#    ALORS concaténer classe avec " spécialiste"


##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'classe' in locals() and isinstance(classe, str), "la variable classe doit être définie et être une chaîne de caractères"
assert 'prenom' in locals() and isinstance(prenom, str) and len(prenom) >= 3, "la variable prenom doit être définie et être une chaîne de caractères d'au moins 3 caractères"
assert 'force' in locals() and isinstance(force, int) and 3 <= force <= 18, "la variable force doit être définie et être un nombre entier compris entre 3 et 18"
assert 'intelligence' in locals() and isinstance(intelligence, int) and 3 <= intelligence <= 18, "la variable intelligence doit être définie et être un nombre entier compris entre 3 et 18"
assert 'dexterite' in locals() and isinstance(dexterite, int) and 3 <= dexterite <= 18, "la variable dexterite doit être définie et être un nombre entier compris entre 3 et 18"
assert 'mage de combat' in classe and force >= 12 and intelligence >= 12, "la classe devrait être 'mage de combat' car la force et l'intelligence sont supérieures ou égales à 12"
assert 'assassin' in classe and intelligence >= 12 and dexterite >= 12, "la classe devrait être 'assassin' car l'intelligence et la dexterite sont supérieures ou égales à 12"
assert not ((force >= 15 or intelligence >= 15 or dexterite >= 15) != "spécialiste" in classe), "notre personnage n'est pas un spécialiste (classe) alors qu'une des trois caractéristiques est supérieure ou égale à 15"

print('FEUILLE DE PERSONNAGE DE "' + prenom + '" :\n\n')
print('FORCE :\t' + force)
print('INTEL :\t' + intelligence)
print('DEXT :\t' + dexterite)
print('--------------')
print('CLASSE : ' + classe)