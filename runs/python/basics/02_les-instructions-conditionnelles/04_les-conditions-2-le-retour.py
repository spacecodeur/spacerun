# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# dans le cadre de cet exercice, seules les quatres lignes ci-dessous peuvent être modifiées

temperature = 25
heure = 14
temps = "ensoleillé"
vent = "faible"

# NE MODIFIE PAS LES LIGNES SUIVANTES

activiteExterieure = "dimanche-maison"

if temperature >= 22:
    if heure >= 10 and heure <= 16 and temps == "ensoleillé":
        activiteExterieure = "baignade"
    else:
        activiteExterieure = "lecture à l'ombre"
elif temperature > 15 and temperature < 30:
    if heure >= 12 and heure <= 18 and temps != "pluvieux":
        activiteExterieure = "randonnée"
    else:
        activiteExterieure = "musée"
else:
    if (temps == "pluvieux" or vent == "fort") and heure > 20:
        activiteExterieure = "cinéma"
    else:
        activiteExterieure = "café"

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'temperature' in locals(), "La variable 'temperature' n'est pas définie"
assert isinstance(temperature, int), "La variable 'temperature' doit être de type int ou float"
assert 'heure' in locals(), "La variable 'heure' n'est pas définie"
assert isinstance(heure, int), "La variable 'heure' doit être de type int"
assert 'temps' in locals(), "La variable 'temps' n'est pas définie"
assert isinstance(temps, str), "La variable 'temps' doit être de type str"
assert 'vent' in locals(), "La variable 'vent' n'est pas définie"
assert isinstance(vent, str), "La variable 'vent' doit être de type str"

assert activiteExterieure == "cinéma", "à la fin du progamme, activité_extérieure doit avoir comme valeur 'cinéma', trouvez la bonne combinaison de valeur des variables temperaature/heure/temps/vent"
