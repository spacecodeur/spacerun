# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

# Dans le cadre de cet exercice, seules les deux lignes ci-dessous peuvent être modifiées

pokemon = None
pv = -10

# NE MODIFIE PAS LES LIGNES SUIVANTES

if pokemon == "magicarpe":
    attaque = "trempette"

isKO = True

if pv > 0:
    isKO = False
    

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert 'pokemon' in locals(), "la variable pokemon doit être définie"
assert isinstance(pokemon, str), "pokemon doit être une chaine de caractère"
assert 'pv' in locals(), "la variable pv doit être définie"
assert isinstance(pv, int), "pv doit être un nombre entier"
assert 'attaque' in locals(), "la variable attaque n'est pas déclarée car la condition pokemon == \"magicarpe\" n'est pas respectée. "
assert isKO == True, "damned, ton magicarpe est aux portes de la mort ! c'est pas cool ça, augmente subtilement ses pv !"

print(pokemon + ' lance l\'attaque ' + attaque + " !")
print('chrysacier riposte en face avec l\'attaque armure , ' + pokemon + ' est toujours debout malgré tout ! quel combat incroyable mes amis !')