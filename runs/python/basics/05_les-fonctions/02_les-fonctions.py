# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analysez/corrigez/complétez le code ci-dessous #
##################################################

def soustraction(a: int, b: int) -> int:
    return a - b

# définir une fonction nommée 'multiplication', elle prend en paramètres deux entiers et retourne le résultat de la multiplication des deux entiers

# définir une fonction nommée 'multiplication3x', elle prend en paramètres trois entiers et retourne le résultat de la multiplication des trois entiers

# définir une fonction nommée 'divisionEntiere', elle prend en paramètres deux entiers et retourne le résultat de la division entière des deux entiers

# définir une fonction nommée 'resteApresDivision', elle prend en paramètres deux entiers et retourne le reste de la division euclidienne des deux entiers


##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# vous pouvez les lire pour vous aider           #
# Mais ne les modifiez pas !                     #
##################################################

assert callable(soustraction), "La fonction soustraction n'est pas définie"
assert soustraction(21, 3) == 18
assert callable(multiplication), "La fonction soustraction n'est pas définie"
assert multiplication(10, 15) == 150
assert callable(multiplication3x), "La fonction soustraction n'est pas définie"
assert multiplication3x(3, 4, 6) == 72
assert callable(divisionEntiere), "La fonction soustraction n'est pas définie"
assert divisionEntiere(13, 5) == 2
assert callable(resteApresDivision), "La fonction soustraction n'est pas définie"
assert resteApresDivision(14, 4) == 2