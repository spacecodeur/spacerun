# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analysez/corrigez/complétez le code ci-dessous #
##################################################

# créer une fonction nommée hello, cette fonction doit retourner la chaine de caractère "bonjour !"


##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# vous pouvez les lire pour vous aider           #
# Mais ne les modifiez pas !                     #
##################################################

assert 'hello' in globals(), "La fonction hello n'existe pas."
assert hello() != None, "Attention à bien utiliser l'instruction return à la fin de la fonction pour retourner la chaine de caratère attendue"
assert hello() == "bonjour !", "La fonction hello ne retourne pas la bonne chaîne de caractères."
