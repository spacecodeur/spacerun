# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analysez/corrigez/complétez le code ci-dessous #
##################################################

def retournerBonjour() -> str:
    return "Bonjour tout le monde !"

# compléter seulement la ligne ci-dessous
messageDeBienvenue = 

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# vous pouvez les lire pour vous aider           #
# Mais ne les modifiez pas !                     #
##################################################

assert messageDeBienvenue == "Bonjour tout le monde !", "la variable messageDeBienvenue doit contenir la valeur \"Bonjour tout le monde !\""