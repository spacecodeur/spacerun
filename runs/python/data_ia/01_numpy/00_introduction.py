# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

import numpy as np

##################################################
# INTRODUCTION                                   #
##################################################

# NumPy est une bibliothèque Python cruciale pour le 
# calcul numérique et la manipulation de données.
# Elle permet de manipuler efficacement des tableaux python
# et offre des fonctions très pratiques pour 
# effectuer des opérations mathématiques et statistiques.

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

simpleArray = [10, 22, 33, 14]
simpleArrayInNumpyFormat = np.array(simpleArray)

firstElement10 = simpleArrayInNumpyFormat[None] # remplacer None par la bonne valeur
thirdElement33 = simpleArrayInNumpyFormat[None] # remplacer None par la bonne valeur

TwoDimensionnalArrayInNumpyFormat = np.array([
    [10, 5, 9],
    [4, 6, 7],
    [8, 2, 1]
])

elements6And7 = TwoDimensionnalArrayInNumpyFormat[1, None] # remplacer None par la bonne valeur
elements30And31And32 = TwoDimensionnalArrayInNumpyFormat[None] # remplacer None par la bonne valeur

ThreeDimensionnalArrayInNumpyFormat = np.array([
    [
        [1, 2, 3], 
        [4, 5, 6]
    ],
    [
        [7, 8, 9], 
        [10, 11, 12]
    ],
    [
        [13, 14, 15], 
        [16, 17, 18]
    ]
])

elements4And5And6 = ThreeDimensionnalArrayInNumpyFormat[None, 1, None] # remplacer None par la bonne valeur
elements16And17And18 = ThreeDimensionnalArrayInNumpyFormat[None] # remplacer None par la bonne valeur

# Et aller, faisons l'exercice inverse now :D

mysteriousArrayInNumpyFormat = np.array([
    [None, None, None], # remplacer None par les bonnes valeurs, quelles bonnes valeurs ? jetez un oeil au reste du code ! ()
    [None, None, None], # attention, seul les None peuvent être modifié ici :)
])

elements7And22 = mysteriousArrayInNumpyFormat[1, 2]
elements0And1And2 = mysteriousArrayInNumpyFormat[0]

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

assert firstElement10 == 10, "firstElement10 à pour valeur " + str(firstElement10) + " et non 10 comme c'est attendu"
assert thirdElement33 == 33, "thirdElement33 à pour valeur " + str(thirdElement33) + " et non 33 comme c'est attendu"
assert elements6And7 == [6, 7], "elements6And7 a pour valeur " + str(elements6And7) + " et non [6, 7] comme c'est attendu"
assert elements30And31And32 == [30, 31, 32], "elements30And31And32 a pour valeur " + str(elements30And31And32) + " et non [30, 31, 32] comme c'est attendu"
assert elements4And5And6 == [4, 5, 6], "elements4And5And6 a pour valeur " + str(elements4And5And6) + " et non [4, 5, 6] comme c'est attendu"
assert elements16And17And18 == [16, 17, 18], "elements16And17And18 a pour valeur " + str(elements16And17And18) + " et non [16, 17, 18] comme c'est attendu"
assert elements7And22 == [7, 22], "elements16And17And18 a pour valeur " + str(elements7And22) + " et non [7, 22] comme c'est attendu"
assert elements0And1And2 == [0, 1, 2], "elements0And1And2 a pour valeur " + str(elements0And1And2) + " et non [0, 1, 2] comme c'est attendu"