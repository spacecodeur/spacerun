# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# INTRODUCTION                                   #
##################################################

##################################################
# Analyse/corrige/complète le code ci-dessous    #
##################################################

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# tu peux les lire pour t'aider à compléter      #
# l'exercice, mais...                            #
# IL NE PAS MODIFIER LE CODE CI-DESSOUS          #
##################################################

tableau_test_1 = np.array([1, 2, 3, 4, 5])
assert calcul_moyenne(tableau_test_1) == 3.0, "Test 1 échoué"
tableau_test_2 = np.array([10, 20, 30, 40, 50])
assert calcul_moyenne(tableau_test_2) == 30.0, "Test 2 échoué"
