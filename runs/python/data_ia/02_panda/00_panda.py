# DELETE THIS COMMENT FOR PASSING TO THE NEXT EXERCICE

##################################################
# Analysez/corrigez/complétez le code ci-dessous #
##################################################

import pandas as pd

def creer_dataframe(liste_dictionnaires):
    """
    Crée un DataFrame à partir d'une liste de dictionnaires.
    
    Args:
        liste_dictionnaires (list): Liste de dictionnaires contenant les données.
        
    Returns:
        pd.DataFrame: Le DataFrame créé.
    """
    # À compléter : Créer un DataFrame à partir de la liste de dictionnaires
    dataframe = pd.DataFrame(liste_dictionnaires)
    
    return dataframe

##################################################
# Ci-dessous, Les tests exécutés pour ce fichier #
# vous pouvez les lire pour vous aider           #
# Mais ne les modifiez pas !                     #
##################################################

donnees_test_1 = [
    {'Nom': 'Alice', 'Âge': 30, 'Ville': 'Paris'},
    {'Nom': 'Bob', 'Âge': 25, 'Ville': 'Londres'},
    {'Nom': 'Charlie', 'Âge': 35, 'Ville': 'New York'}
]
assert creer_dataframe(donnees_test_1).equals(pd.DataFrame(donnees_test_1)), "Test 1 échoué"

donnees_test_2 = [
    {'Nom': 'David', 'Âge': 40, 'Ville': 'Berlin'},
    {'Nom': 'Eva', 'Âge': 22, 'Ville': 'Tokyo'}
]
assert creer_dataframe(donnees_test_2).equals(pd.DataFrame(donnees_test_2)), "Test 2 échoué"
