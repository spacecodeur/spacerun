# Spacerun

Une petite application pour speedrunner/spacerunner des exercices sur un langage de programmation donné.

## Initialiser le projet

0. installer [python](https://www.python.org/downloads/)
> vérifier la bonne installation de python depuis un terminal ; la commande `python3 --version` (ou `python --version` sur windows) ne doit pas retourner d'erreur
1. installer [git](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)
> vérifier la bonne installation de git depuis un terminal ; la commande `git --version` ne doit pas retourner d'erreur
2. depuis son terminal préféré, se déplacer à la racine du dossier qui contient vos projets 
3. cloner ce repo avec la commande : `git clone git@gitlab.com:spacecodeur/spacerun.git`
4. (ne surtout pas oublier de) se déplacer dans le dossier spacerun : `cd spacerun`
5. installer un environnement virtuel 'vendv' : `python3 -m venv .venv` (ou `python -m venv .venv` sur windows)
> un nouveau dossier `.venv` devrait maintenant se trouver dans le dossier 'spacerun'

> un environnement virtuel 'venv' permet d'installer des packages python localement dans un dossier (et non globalement sur l'ordinateur, pratique si on veut éviter de polluer son ordinateur de packages python qui ne sont utilisés que dans un projet spécifique)

## Démarrer une série d'exercices

6. vérifier que vous avez bien un terminal ouvert et situé dans le dossier 'spacerun'
7. activer l'environnement virtuel venv avec la commande `source .venv/bin/activate` (ou `.venv\Scripts\activate` sur windows)

8. installer les packages nécessaires pour le bon fonctionnement de spacerun : `pip install -r requirements.txt`
9. (et enfin) démarrez l'application 'spacerun' via la commande : `python main.py`

Besoin de sortir de l'environnement virtuel 'venv' depuis votre terminal ? utilisez simplement la commande `deactivate` (reprendre à partir de l'étape 6 si vous quittez l'environnement virtuel 'venv')


## (mode mainteneur/dev) Mettre à jour le requirements.txt

Ce fichier est un équivalent, en python, des fichiers de gestion de version tels que packages.json (node/js) ou encore composer.json (composer/php)

Pour le mettre à jour en fonction des dépendances utilisées dans le code, voici la commande : `pip freeze > requirements.txt`